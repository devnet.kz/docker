# Dockerfiles

[Docker hub | Devnet.kz](https://hub.docker.com/u/devnetkz)

## node.js

### version: 18

#### development
```bash
make env=development image=node version=18 cmd=build-push
```

#### production
```bash
make env=production image=node version=18 cmd=build-push
```

### version: 19

#### development
```bash
make env=development image=node version=19 cmd=build-push
```

#### production
```bash
make env=production image=node version=19 cmd=build-push
```

### version: 20

#### development
```bash
make env=development image=node version=20 cmd=build-push
```

#### production
```bash
make env=production image=node version=20 cmd=build-push
```

## codeception

### version: 5

#### development
```bash
make env=development image=codeception version=5 cmd=build-push
```
